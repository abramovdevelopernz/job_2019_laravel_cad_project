<?php
//https://laravel-news.com/eloquent-tips-tricks
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Pagination\Paginator;

class User_Controller extends Controller
{


    public function directory_list_load_more()
    {
        /*
        */






                $users = DB::table('users')
                ->join('user_types', 'users.user_type_id', '=', 'user_types.id')
                ->join('regions', 'users.region_id', '=', 'regions.id')
                ->where('user_type_id', '=', 1)
                ->orderBy("users.name","ASC")
                ->paginate(10);
                return response()-> json($users);
    }

          public function index_pagination_load_more()
          {
              $users = User::orderBy("id","DESC")->paginate(10);
              return response()-> json($users);
          }

          public function index_pagination()
          {
              //http://laravel.su/docs/5.2/queries

             // https://www.youtube.com/watch?v=P8Ic_kTv3Xg
             // $users = DB::table('users')->paginate('12')
            //  ->paginate('12')
                //  ->join('user_types', 'users.user_type_id', '=', 'user_types.id')
                //  ->join('regions', 'users.region_id', '=', 'regions.id')

                  //!->where('posts.title', 'like', '%' . $searchText . '%')
                  //!->orWhere('categories.title', 'like', '%' . $searchText . '%')
                //  ->where('user_type_id', '=', 1)



               //   ->select(
              //        'users.id',
             //         'users.email',
              //        'users.name',
             //         'users.notes',
             //         'users.city',
             //         'users.address',
              //        'users.phone',
             //         'users.website',
              //        'regions.region',
              //        'user_types.user_type'
              //    )
                  //   ->paginate(20)
             //     ->get();


          //    dd($users);
           //   return response()->json($users);


              $result = User::paginate(10);
              return $result;



          }






          /**
           * Display a listing of the resource.
           *
           * @return \Illuminate\Http\Response
           */
    public function index()
    {
        //http://laravel.su/docs/5.2/queries
        $users = DB::table('users')

            ->join('user_types', 'users.user_type_id', '=', 'user_types.id')
            ->join('regions', 'users.region_id', '=', 'regions.id')

            //->where('posts.title', 'like', '%' . $searchText . '%')
            //->orWhere('categories.title', 'like', '%' . $searchText . '%')
            ->where('user_type_id', '=', 1)

            ->orderBy('users.name', 'ASC')

            ->select(
                'users.id',
                'users.email',
                'users.name',
                'users.notes',
                'users.city',
                'users.address',
                'users.phone',
                'users.website',
                'regions.region',
                'user_types.user_type'
                    )
         //   ->paginate(20)
            ->get();

        return response()->json($users);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //$user = User::findOrFail($id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        return User::findorfail($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
