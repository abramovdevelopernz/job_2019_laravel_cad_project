//https://www.youtube.com/watch?v=P8Ic_kTv3Xg
//https://www.npmjs.com/package/react-js-pagination
//Необходимо установит все зависимости через NPM
import React, { Component } from 'react';
import axios from 'axios';
import {Link} from 'react-router-dom';         //npm install --save react-router-dom
import Pagination from "react-js-pagination"; //$ npm install react-js-pagination
import ReactDOM from 'react-dom';

export default class Directory_pagination extends Component {

    //Возникает при загрузке
    constructor(){
        super();
        this.myRef = React.createRef();
        this.state = {
            users: [],
        }
        this.handlePageChange = this.handlePageChange.bind(this);
    }
    componentDidMount() {
        axios.get('http://127.0.0.1:8000/directory_pagination_list')
            .then(response => {
                this.setState({
                    users:response.data.data,
                    totalItemsCount:response.data.total,
                });
        });

    }

    handlePageChange(pageNumber) {
        console.log(`active page is ${pageNumber}`);
        axios.get('http://127.0.0.1:8000/directory_pagination_list?page='+pageNumber)
            .then(response=>{
                this.setState({
                    users:response.data.data,
                    itemsCountPerPage:response.data.per_page,
                    totalItemsCount:response.data.total,
                    activePage:response.data.current_page,

                });
                //window.scrollTo(0,0);
                document.getElementById('company_brend').scrollIntoView();
            });
    }

    render() {
        return (

            <div className="container">

                <div className="col-md-3 no-padd">
                    <div className="input-group">
                        <select id="choose-l" className="form-control">
                            <option>Choose Location</option>
                            {this.state.users.map(user =>

                            <option>{user.name}</option>

                                )}
                            
                        </select>
                    </div>
                </div>


                <Pagination
                    activePage={this.state.activePage}              //Required. Active page
                    itemsCountPerPage={10}                          //Count of items per page
                    totalItemsCount={this.state.totalItemsCount}    //Required. Total count of items which you are going to display
                    pageRangeDisplayed={5}                          //Количество кнопок с цифрами в компоненте
                    onChange={this.handlePageChange}
                />

                {this.state.users.map(user =>
                    <div className="item-click" key={user.id}>
                        <article>
                            <div className="brows-company">
                                <div className="col-md-2 col-sm-2">
                                    <div className="brows-company-pic">
                                        <a href="company-detail.html"><img src="http://via.placeholder.com/150x150"
                                                                           className="img-responsive" alt=""></img></a>
                                    </div>
                                </div>
                                <div className="col-md-4 col-sm-4">
                                    <div className="brows-company-name">
                                        <a href="company-detail.html"><h4>{user.name}</h4></a>
                                        <span className="brows-company-tagline">{user.website}   {user.email}</span>
                                    </div>
                                </div>
                                <div className="col-md-4 col-sm-4">
                                    <div className="brows-company-location">
                                        <p><i className="fa fa-map-marker"></i> {user.address} {user.city} {user.region}</p>
                                    </div>
                                </div>
                                <div className="col-md-2 col-sm-2">
                                    <div className="brows-company-position">
                                        <p>{user.phone}</p>
                                    </div>
                                </div>


                                <div className="col-md-12 col-sm-12">
                                    <div className="brows-company-name">
                                        <a href="company-detail.html"><h4>{user.notes}</h4></a>
                                    </div>
                                </div>


                            </div>
                        </article>
                    </div>

                )}

                <Pagination
                    activePage={this.state.activePage}              //Required. Active page
                    itemsCountPerPage={10}                          //Count of items per page
                    totalItemsCount={this.state.totalItemsCount}    //Required. Total count of items which you are going to display
                    pageRangeDisplayed={5}                          //Количество кнопок с цифрами в компоненте
                    onChange={this.handlePageChange}
                />

            </div>
        );
    }
}

if (document.getElementById('directory_pagination')) {
    ReactDOM.render(<Directory_pagination />, document.getElementById('directory_pagination'));
}