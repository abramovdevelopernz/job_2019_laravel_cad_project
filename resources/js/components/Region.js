import React, { Component } from 'react';
import axios from 'axios';
import ReactDOM from 'react-dom';

export default class Region extends Component {

    constructor(){
        super();
        this.state = {
            regions: [],
        }
    }

    componentDidMount() {
        axios.get('/region_list')
            .then(response => {
                this.setState({
                    regions:response.data,
                });
            });
    }

    render() {
        return (

                <div className="col-md-3 no-padd">
                    <div className="input-group">
                        <select id="region_id" className="form-control choose_fill">

                            <option>Choose Location</option>

                            {this.state.regions.map(region =>
                                <option key={region.region} value={region.id}>{region.region}</option>
                            )}

                        </select>
                    </div>
                </div>

        );
    }
}

if (document.getElementById('region')) {
    ReactDOM.render(<Region />, document.getElementById('region'));
}