//https://www.youtube.com/watch?v=P8Ic_kTv3Xg
//https://www.npmjs.com/package/react-js-pagination
//Необходимо установит все зависимости через NPM
import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import axios from 'axios';
import {Link} from 'react-router-dom';         //npm install --save react-router-dom
import Pagination from "react-js-pagination"; //$ npm install react-js-pagination


export default class Directory_pagination_load_more extends Component {

    //Возникает при загрузке
    constructor(){
        super();
        this.state = {
            data: [],
            url:'http://127.0.0.1:8000/directory_pagination_list_load_more',
            pagination:[]
        }
    }


    componentWillMount() {
        this.fetchUsers()
    }


    fetchUsers(){

        let $this = this;

        axios.get(this.state.url)
            .then(response => {
                this.setState({
                    data: $this.state.data.length > 0 ? this.state.data.concat(response.data.data):response.data.data,
                    url: response.data.next_page_url
                });
                $this.makePagination(response.data);
            });
    }


    loadMore() {
        this.setState({
            url:this.state.pagination.next_page_url
        })
        this.fetchUsers();
    }



    makePagination(data) {
        let pagination = {
            current_page: data.current_page,
            last_page: data.last_page,
            next_page_url: data.next_page_url,
            prev_page_url: data.prev_page_url,
        };
        this.setState({
            pagination: pagination
        })
    }

    render() {
        return (
            <div className="container">


                {this.state.data.map(data =>
                    <div className="item-click" key={data.id}>
                        <article>
                            <div className="brows-company">
                                <div className="col-md-2 col-sm-2">
                                    <div className="brows-company-pic">
                                        <a href="company-detail.html"><img src="http://via.placeholder.com/150x150"
                                                                           className="img-responsive" alt=""></img></a>
                                    </div>
                                </div>
                                <div className="col-md-4 col-sm-4">
                                    <div className="brows-company-name">
                                        <a href="company-detail.html"><h4>{data.name}</h4></a>
                                        <span className="brows-company-tagline">{data.website}   {data.email}</span>
                                    </div>
                                </div>
                                <div className="col-md-4 col-sm-4">
                                    <div className="brows-company-location">
                                        <p><i className="fa fa-map-marker"></i> {data.address} {data.city} {data.region}</p>
                                    </div>
                                </div>
                                <div className="col-md-2 col-sm-2">
                                    <div className="brows-company-position">
                                        <p>{data.phone}</p>
                                    </div>
                                </div>


                                <div className="col-md-12 col-sm-12">
                                    <div className="brows-company-name">
                                        <a href="company-detail.html"><h4>{data.notes}</h4></a>
                                    </div>
                                </div>


                            </div>
                        </article>
                    </div>

                )}

                <div className="col-md-12 col-sm-12">
                    <div className="text-center">
                        <a className="btn btn-primary" onClick={this.loadMore.bind(this)}>Load More</a>
                    </div>
                </div>

            </div>
        );
    }
}

if (document.getElementById('directory_pagination_load_more')) {
    ReactDOM.render(<Directory_pagination_load_more />, document.getElementById('directory_pagination_load_more'));
}