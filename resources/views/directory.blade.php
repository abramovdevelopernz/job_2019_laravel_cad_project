@extends('layouts.app')

@section('content')

    <!-- Main Banner Section Start -->
    <div class="banner" style="background-image:url(http://via.placeholder.com/1920x850);">
        <div class="container">
            <div class="banner-caption">
                <div class="col-md-12 col-sm-12 banner-text">
                    <h1>Direcory</h1>
                    <form class="form-horizontal">


                        <div class="col-md-3 no-padd">
                            <div class="input-group">
                                <select id="discipline_id" class="form-control choose_fill">
                                    <option>Choose Discipline</option>
                                    <option>Dscipline 1</option>
                                    <option>Dscipline 2</option>
                                    <option>Dscipline 3</option>
                                </select>
                            </div>
                        </div>

                        <div class="col-md-3 no-padd">
                            <div class="input-group">
                                <select id="speciality_id" class="form-control choose_fill">
                                    <option>Choose Speciality</option>
                                    <option>Speciality 1</option>
                                    <option>Speciality 2</option>
                                    <option>Speciality 3</option>
                                </select>
                            </div>
                        </div>



                        <div id="region"></div>


                        <div class="col-md-3 no-padd">
                            <div class="input-group">
                                <select id="region_id_test" class="form-control choose_fill">
                                    <option>Choose Location</option>
                                    <option>111</option>
                                    <option>222</option>
                                </select>
                            </div>
                        </div>



                        <div class="col-md-3 no-padd">
                            <div class="input-group">
                                <input type="text" class="form-control right-bor" id="user_name" placeholder="Company name">
                            </div>
                        </div>





                        <div class="col-md-12 no-padd">
                            <div class="input-group">
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <div class="company-brand">
            <div class="container">
                <div id="company-brands" class="owl-carousel">
                    <div class="brand-img">
                        <img src="assets/img/microsoft-home.png" class="img-responsive" alt="" />
                    </div>
                    <div class="brand-img">
                        <img src="assets/img/img-home.png" class="img-responsive" alt="" />
                    </div>
                    <div class="brand-img">
                        <img src="assets/img/mothercare-home.png" class="img-responsive" alt="" />
                    </div>
                    <div class="brand-img">
                        <img src="assets/img/paypal-home.png" class="img-responsive" alt="" />
                    </div>
                    <div class="brand-img">
                        <img src="assets/img/serv-home.png" class="img-responsive" alt="" />
                    </div>
                    <div class="brand-img">
                        <img src="assets/img/xerox-home.png" class="img-responsive" alt="" />
                    </div>
                    <div class="brand-img">
                        <img src="assets/img/yahoo-home.png" class="img-responsive" alt="" />
                    </div>
                    <div class="brand-img">
                        <img src="assets/img/mothercare-home.png" class="img-responsive" alt="" />
                    </div>
                </div>
            </div>
        </div>

    </div>
    <div class="clearfix"></div>


    <section class="pricing">
        <div class="container">
            <div class="row">
                <div id="directory_list_load_more"></div>
            </div>
        </div>
    </section>

@endsection