@extends('layouts.app')

@section('content')

    <!-- Main Banner Section Start -->
    <div class="banner" style="background-image:url(http://via.placeholder.com/1920x850);">
        <div class="container">
            <div class="banner-caption">
                <div class="col-md-12 col-sm-12 banner-text">
                    <h1>News</h1>
                    <form class="form-horizontal">
                        <div class="col-md-10 no-padd">
                            <div class="input-group">
                                <input type="text" class="form-control right-bor" id="joblist" placeholder="Text">
                            </div>
                        </div>





                        <div class="col-md-2 no-padd">
                            <div class="input-group">
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <div class="company-brand">
            <div class="container">
                <div id="company-brands" class="owl-carousel">
                    <div class="brand-img">
                        <img src="assets/img/microsoft-home.png" class="img-responsive" alt="" />
                    </div>
                    <div class="brand-img">
                        <img src="assets/img/img-home.png" class="img-responsive" alt="" />
                    </div>
                    <div class="brand-img">
                        <img src="assets/img/mothercare-home.png" class="img-responsive" alt="" />
                    </div>
                    <div class="brand-img">
                        <img src="assets/img/paypal-home.png" class="img-responsive" alt="" />
                    </div>
                    <div class="brand-img">
                        <img src="assets/img/serv-home.png" class="img-responsive" alt="" />
                    </div>
                    <div class="brand-img">
                        <img src="assets/img/xerox-home.png" class="img-responsive" alt="" />
                    </div>
                    <div class="brand-img">
                        <img src="assets/img/yahoo-home.png" class="img-responsive" alt="" />
                    </div>
                    <div class="brand-img">
                        <img src="assets/img/mothercare-home.png" class="img-responsive" alt="" />
                    </div>
                </div>
            </div>
        </div>

    </div>
    <div class="clearfix"></div>
    <!-- Main Banner Section End -->



    <section class="section">
        <div class="container">
            <div class="row .no-mrg">
                <!-- Start Blogs -->
                <div class="col-md-12">

                    <article class="blog-news">
                        <div class="short-blog">
                            <figure class="img-holder">
                                <a href="blog-detail.html"><img src="assets/img/blog/1.jpg" class="img-responsive" alt="News"></a>
                                <div class="blog-post-date">
                                    Mar 12, 2017
                                </div>
                            </figure>
                            <div class="blog-content">
                                <div class="post-meta">By: <span class="author">Daniel Dax</span> | 10 Comments </div>
                                <a href="blog-detail.html"><h2>Helping Kids Grow Up Stronger</h2></a>
                                <div class="blog-text">
                                    <p>Consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore</p>
                                    <div class="post-meta">Filed Under: <span class="category"><a href="#">Technology</a></span></div>
                                </div>
                            </div>
                        </div>
                    </article>

                    <article class="blog-news">
                        <div class="short-blog">
                            <figure class="img-holder">
                                <a href="blog-detail.html"><img src="assets/img/blog/2.jpg" class="img-responsive" alt="News"></a>
                                <div class="blog-post-date">
                                    Mar 12, 2017
                                </div>
                            </figure>
                            <div class="blog-content">
                                <div class="post-meta">By: <span class="author">Daniel Dax</span> | 10 Comments </div>
                                <a href="blog-detail.html"><h2>Helping Kids Grow Up Stronger</h2></a>
                                <div class="blog-text">
                                    <p>Consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore</p>
                                    <div class="post-meta">Filed Under: <span class="category"><a href="#">Technology</a></span></div>
                                </div>
                            </div>
                        </div>
                    </article>

                    <article class="blog-news">
                        <div class="short-blog">
                            <figure class="img-holder">
                                <a href="blog-detail.html"><img src="assets/img/blog/3.jpg" class="img-responsive" alt="News"></a>
                                <div class="blog-post-date">
                                    Mar 12, 2017
                                </div>
                            </figure>
                            <div class="blog-content">
                                <div class="post-meta">By: <span class="author">Daniel Dax</span> | 10 Comments </div>
                                <a href="blog-detail.html"><h2>Helping Kids Grow Up Stronger</h2></a>
                                <div class="blog-text">
                                    <p>Consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore</p>
                                    <div class="post-meta">Filed Under: <span class="category"><a href="#">Technology</a></span></div>
                                </div>
                            </div>
                        </div>
                    </article>
                </div>
                <!-- End Blogs -->

            </div>
            <div class="row">
                <ul class="pagination">
                    <li><a href="#">«</a></li>
                    <li class="active"><a href="#">1</a></li>
                    <li><a href="#">2</a></li>
                    <li><a href="#">3</a></li>
                    <li><a href="#">4</a></li>
                    <li><a href="#"><i class="fa fa-ellipsis-h"></i></a></li>
                    <li><a href="#">»</a></li>
                </ul>
            </div>
        </div>
    </section>



@endsection