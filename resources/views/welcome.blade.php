@extends('layouts.app')

@section('content')

    <!-- Main Banner Section Start -->
    <section class="inner-header-title" style="background-image:url(http://via.placeholder.com/1920x850);">
        <div class="container">
            <h1>Mandefinder.co.nz</h1>
        </div>
    </section>

    <div class="clearfix"></div>
    <!-- Main Banner Section End -->


    <!-- Directories Section Start -->
    <section class="pricing">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="main-heading">
                        <p></p>
                        <h2> <span>Directories</span></h2>
                    </div>
                </div>
            </div>
            <!--/row-->

            <div class="row">
                    <!-- Single Browse Company -->
                    <div class="item-click">
                        <article>
                            <div class="brows-company">
                                <div class="col-md-2 col-sm-2">
                                    <div class="brows-company-pic">
                                        <img src="http://via.placeholder.com/150x150" class="img-responsive" alt="">
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-4">
                                    <div class="brows-company-name">
                                        <a href="company-detail.html"><h4>AutoDesk</h4></a>
                                        <span class="brows-company-tagline">Software Company</span>
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-4">
                                    <div class="brows-company-location">
                                        <p><i class="fa fa-map-marker"></i> FALBROOK NSW 2330</p>
                                    </div>
                                </div>
                                <div class="col-md-2 col-sm-2">
                                    <div class="brows-company-position">
                                        <p>6 Opening</p>
                                    </div>
                                </div>
                            </div>
                        </article>
                    </div>

                    <!-- Single Browse Company -->
                    <div class="item-click">
                        <article>
                            <div class="brows-company">
                                <div class="col-md-2 col-sm-2">
                                    <div class="brows-company-pic">
                                        <a href="company-detail.html"><img src="http://via.placeholder.com/150x150" class="img-responsive" alt=""></a>
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-4">
                                    <div class="brows-company-name">
                                        <a href="company-detail.html"><h4>Google</h4></a>
                                        <span class="brows-company-tagline">Software Company</span>
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-4">
                                    <div class="brows-company-location">
                                        <p><i class="fa fa-map-marker"></i> Street #210, Make New London</p>
                                    </div>
                                </div>
                                <div class="col-md-2 col-sm-2">
                                    <div class="brows-company-position">
                                        <p>6 Opening</p>
                                    </div>
                                </div>
                            </div>
                        </article>
                    </div>

                    <!-- Single Browse Company -->
                    <div class="item-click">
                        <article>
                            <div class="brows-company">
                                <div class="col-md-2 col-sm-2">
                                    <div class="brows-company-pic">
                                        <a href="company-detail.html"><img src="http://via.placeholder.com/150x150" class="img-responsive" alt=""></a>
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-4">
                                    <div class="brows-company-name">
                                        <a href="company-detail.html"><h4>Honda</h4></a>
                                        <span class="brows-company-tagline">Motor Agency</span>
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-4">
                                    <div class="brows-company-location">
                                        <p><i class="fa fa-map-marker"></i> BULLAROOK VIC 3352</p>
                                    </div>
                                </div>
                                <div class="col-md-2 col-sm-2">
                                    <div class="brows-company-position">
                                        <p>6 Opening</p>
                                    </div>
                                </div>
                            </div>
                        </article>
                    </div>

                    <!-- Single Browse Company -->
                    <div class="item-click">
                        <article>
                            <div class="brows-company">
                                <div class="col-md-2 col-sm-2">
                                    <div class="brows-company-pic">
                                        <a href="company-detail.html"><img src="http://via.placeholder.com/150x150" class="img-responsive" alt=""></a>
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-4">
                                    <div class="brows-company-name">
                                        <a href="company-detail.html"><h4>Microsoft</h4></a>
                                        <span class="brows-company-tagline">Software Company</span>
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-4">
                                    <div class="brows-company-location">
                                        <p><i class="fa fa-map-marker"></i> Street #210, Make New London</p>
                                    </div>
                                </div>
                                <div class="col-md-2 col-sm-2">
                                    <div class="brows-company-position">
                                        <p>6 Opening</p>
                                    </div>
                                </div>
                            </div>
                        </article>
                    </div>

            </div>

            <!-- Single Freelancer -->
            <div class="row">
                <div class="col-md-12 col-sm-12">
                    <div class="text-center">
                        <a href="freelancers-2.html" class="btn btn-primary">Load More</a>
                    </div>
                </div>
            </div>

        </div>
    </section>
    <!-- End Directories Section -->





































    <section class="pricing">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="main-heading">
                        <p></p>
                        <h2> <span>Tender</span></h2>
                    </div>
                </div>
            </div>
            <!--/row-->


            <div class="row">


                <div class="item-click">
                    <article>
                        <div class="brows-job-list">
                            <div class="col-md-1 col-sm-2 small-padding">
                                <div class="brows-job-company-img">
                                    <a href="job-detail.html"><img src="http://via.placeholder.com/150x150" class="img-responsive" alt=""></a>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-5">
                                <div class="brows-job-position">
                                    <a href="job-apply-detail.html"><h3>Digital Marketing Manager</h3></a>
                                    <p>
                                        <span>Autodesk</span><span class="brows-job-sallery"><i class="fa fa-money"></i>$750 - 800</span>
                                        <span class="job-type cl-success bg-trans-success">Full Time</span>
                                    </p>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-3">
                                <div class="brows-job-location">
                                    <p><i class="fa fa-map-marker"></i>QBL Park, C40</p>
                                </div>
                            </div>
                            <div class="col-md-2 col-sm-2">
                                <div class="brows-job-link">
                                    <a href="job-apply-detail.html" class="btn btn-default">Apply Now</a>
                                </div>
                            </div>
                        </div>
                        <span class="tg-themetag tg-featuretag">Premium</span>
                    </article>
                </div>

                <div class="item-click">
                    <article>
                        <div class="brows-job-list">
                            <div class="col-md-1 col-sm-2 small-padding">
                                <div class="brows-job-company-img">
                                    <a href="job-detail.html"><img src="http://via.placeholder.com/150x150" class="img-responsive" alt=""></a>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-5">
                                <div class="brows-job-position">
                                    <a href="job-apply-detail.html"><h3>Compensation Analyst</h3></a>
                                    <p>
                                        <span>Google</span><span class="brows-job-sallery"><i class="fa fa-money"></i>$810 - 900</span>
                                        <span class="job-type bg-trans-warning cl-warning">Part Time</span>
                                    </p>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-3">
                                <div class="brows-job-location">
                                    <p><i class="fa fa-map-marker"></i>QBL Park, C40</p>
                                </div>
                            </div>
                            <div class="col-md-2 col-sm-2">
                                <div class="brows-job-link">
                                    <a href="job-apply-detail.html" class="btn btn-default">Apply Now</a>
                                </div>
                            </div>
                        </div>
                    </article>
                </div>

                <div class="item-click">
                    <article>
                        <div class="brows-job-list">
                            <div class="col-md-1 col-sm-2 small-padding">
                                <div class="brows-job-company-img">
                                    <a href="job-detail.html"><img src="http://via.placeholder.com/150x150" class="img-responsive" alt=""></a>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-5">
                                <div class="brows-job-position">
                                    <a href="job-apply-detail.html"><h3>Investment Banker</h3></a>
                                    <p>
                                        <span>Honda</span><span class="brows-job-sallery"><i class="fa fa-money"></i>$800 - 910</span>
                                        <span class="job-type bg-trans-primary cl-primary">Freelancer</span>
                                    </p>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-3">
                                <div class="brows-job-location">
                                    <p><i class="fa fa-map-marker"></i>QBL Park, C40</p>
                                </div>
                            </div>
                            <div class="col-md-2 col-sm-2">
                                <div class="brows-job-link">
                                    <a href="job-apply-detail.html" class="btn btn-default">Apply Now</a>
                                </div>
                            </div>
                        </div>
                        <span class="tg-themetag tg-featuretag">Premium</span>
                    </article>
                </div>

                <div class="item-click">
                    <article>
                        <div class="brows-job-list">
                            <div class="col-md-1 col-sm-2 small-padding">
                                <div class="brows-job-company-img">
                                    <a href="job-detail.html"><img src="http://via.placeholder.com/150x150" class="img-responsive" alt=""></a>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-5">
                                <div class="brows-job-position">
                                    <a href="job-apply-detail.html"><h3>Financial Analyst</h3></a>
                                    <p>
                                        <span>Microsoft</span><span class="brows-job-sallery"><i class="fa fa-money"></i>$580 - 600</span>
                                        <span class="job-type bg-trans-success cl-success">Full Time</span>
                                    </p>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-3">
                                <div class="brows-job-location">
                                    <p><i class="fa fa-map-marker"></i>QBL Park, C40</p>
                                </div>
                            </div>
                            <div class="col-md-2 col-sm-2">
                                <div class="brows-job-link">
                                    <a href="job-apply-detail.html" class="btn btn-default">Apply Now</a>
                                </div>
                            </div>
                        </div>
                    </article>
                </div>







            </div>




















            <!-- Single Freelancer -->
            <div class="row">
                <div class="col-md-12 col-sm-12">
                    <div class="text-center">
                        <a href="freelancers-2.html" class="btn btn-primary">Load More</a>
                    </div>
                </div>
            </div>

        </div>
    </section>




    <section>
    <div class="container">

        <div class="row">
            <div class="main-heading">
                <p>Job</p>
                <h2>Job <span>wanted</span></h2>
            </div>
        </div>




        <div class="card-body">

            <article class="advance-search-job">
                <div class="row no-mrg">
                    <div class="col-md-6 col-sm-6">
                        <a href="new-job-detail.html" title="job Detail">
                            <div class="advance-search-img-box">
                                <img src="assets/img/com-2.jpg" class="img-responsive" alt="">
                            </div>
                        </a>
                        <div class="advance-search-caption">
                            <a href="new-job-detail.html" title="Job Dtail"><h4>Product Designer</h4></a>
                            <span>Google Ltd</span>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-4">
                        <div class="advance-search-job-locat">
                            <p><i class="fa fa-map-marker"></i>QBL Park, C40</p>
                        </div>
                    </div>
                    <div class="col-md-2 col-sm-2">
                        <a href="javascript:void(0)" data-toggle="modal" data-target="#apply-job" class="btn advance-search" title="apply">Apply</a>
                    </div>
                </div>
                <span class="tg-themetag tg-featuretag">Premium</span>
            </article>

            <article class="advance-search-job">
                <div class="row no-mrg">
                    <div class="col-md-6 col-sm-6">
                        <a href="new-job-detail.html" title="job Detail">
                            <div class="advance-search-img-box">
                                <img src="assets/img/com-6.jpg" class="img-responsive" alt="">
                            </div>
                        </a>
                        <div class="advance-search-caption">
                            <a href="new-job-detail.html" title="Job Dtail"><h4>Project Manager</h4></a>
                            <span>Vertue Ltd</span>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-4">
                        <div class="advance-search-job-locat">
                            <p><i class="fa fa-map-marker"></i>QBL Park, C40</p>
                        </div>
                    </div>
                    <div class="col-md-2 col-sm-2">
                        <a href="javascript:void(0)" data-toggle="modal" data-target="#apply-job" class="btn advance-search" title="apply">Apply</a>
                    </div>
                </div>
            </article>

            <article class="advance-search-job">
                <div class="row no-mrg">
                    <div class="col-md-6 col-sm-6">
                        <a href="new-job-detail.html" title="job Detail">
                            <div class="advance-search-img-box">
                                <img src="assets/img/com-7.jpg" class="img-responsive" alt="">
                            </div>
                        </a>
                        <div class="advance-search-caption">
                            <a href="new-job-detail.html" title="Job Dtail"><h4>Database Designer</h4></a>
                            <span>Twitter Ltd</span>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-4">
                        <div class="advance-search-job-locat">
                            <p><i class="fa fa-map-marker"></i>QBL Park, C40</p>
                        </div>
                    </div>
                    <div class="col-md-2 col-sm-2">
                        <a href="#" class="btn applied advance-search" title="applied"><i class="fa fa-check" aria-hidden="true"></i>Applied</a>
                    </div>
                </div>
                <span class="tg-themetag tg-featuretag">Premium</span>
            </article>

            <article class="advance-search-job">
                <div class="row no-mrg">
                    <div class="col-md-6 col-sm-6">
                        <a href="new-job-detail.html" title="job Detail">
                            <div class="advance-search-img-box">
                                <img src="assets/img/com-2.jpg" class="img-responsive" alt="">
                            </div>
                        </a>
                        <div class="advance-search-caption">
                            <a href="new-job-detail.html" title="Job Dtail"><h4>Product Designer</h4></a>
                            <span>Google Ltd</span>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-4">
                        <div class="advance-search-job-locat">
                            <p><i class="fa fa-map-marker"></i>QBL Park, C40</p>
                        </div>
                    </div>
                    <div class="col-md-2 col-sm-2">
                        <a href="javascript:void(0)" data-toggle="modal" data-target="#apply-job" class="btn advance-search" title="apply">Apply</a>
                    </div>
                </div>
            </article>

            <article class="advance-search-job">
                <div class="row no-mrg">
                    <div class="col-md-6 col-sm-6">
                        <a href="new-job-detail.html" title="job Detail">
                            <div class="advance-search-img-box">
                                <img src="assets/img/com-5.jpg" class="img-responsive" alt="">
                            </div>
                        </a>
                        <div class="advance-search-caption">
                            <a href="new-job-detail.html" title="Job Dtail"><h4>Human Resource</h4></a>
                            <span>Skype Ltd</span>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-4">
                        <div class="advance-search-job-locat">
                            <p><i class="fa fa-map-marker"></i>QBL Park, C40</p>
                        </div>
                    </div>
                    <div class="col-md-2 col-sm-2">
                        <a href="#" class="btn applied advance-search" title="applied"><i class="fa fa-check" aria-hidden="true"></i>Applied</a>
                    </div>
                </div>
            </article>

            <article class="advance-search-job">
                <div class="row no-mrg">
                    <div class="col-md-6 col-sm-6">
                        <a href="new-job-detail.html" title="job Detail">
                            <div class="advance-search-img-box">
                                <img src="assets/img/com-6.jpg" class="img-responsive" alt="">
                            </div>
                        </a>
                        <div class="advance-search-caption">
                            <a href="new-job-detail.html" title="Job Dtail"><h4>Project Manager</h4></a>
                            <span>Vertue Ltd</span>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-4">
                        <div class="advance-search-job-locat">
                            <p><i class="fa fa-map-marker"></i>QBL Park, C40</p>
                        </div>
                    </div>
                    <div class="col-md-2 col-sm-2">
                        <a href="javascript:void(0)" data-toggle="modal" data-target="#apply-job" class="btn advance-search" title="applied">Apply</a>
                    </div>
                </div>
                <span class="tg-themetag tg-featuretag">Premium</span>
            </article>

            <article class="advance-search-job">
                <div class="row no-mrg">
                    <div class="col-md-6 col-sm-6">
                        <a href="new-job-detail.html" title="job Detail">
                            <div class="advance-search-img-box">
                                <img src="assets/img/com-7.jpg" class="img-responsive" alt="">
                            </div>
                        </a>
                        <div class="advance-search-caption">
                            <a href="new-job-detail.html" title="Job Dtail"><h4>CEO &amp; Manager</h4></a>
                            <span>Twitter</span>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-4">
                        <div class="advance-search-job-locat">
                            <p><i class="fa fa-map-marker"></i>QBL Park, C40</p>
                        </div>
                    </div>
                    <div class="col-md-2 col-sm-2">
                        <a href="javascript:void(0)" data-toggle="modal" data-target="#apply-job" class="btn advance-search" title="apply">Apply</a>
                    </div>
                </div>
            </article>

            <article class="advance-search-job">
                <div class="row no-mrg">
                    <div class="col-md-6 col-sm-6">
                        <a href="new-job-detail.html" title="job Detail">
                            <div class="advance-search-img-box">
                                <img src="assets/img/com-4.jpg" class="img-responsive" alt="">
                            </div>
                        </a>
                        <div class="advance-search-caption">
                            <a href="new-job-detail.html" title="Job Dtail"><h4>Product Designer</h4></a>
                            <span>Microsoft Ltd</span>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-4">
                        <div class="advance-search-job-locat">
                            <p><i class="fa fa-map-marker"></i>QBL Park, C40</p>
                        </div>
                    </div>
                    <div class="col-md-2 col-sm-2">
                        <a href="#" class="btn applied advance-search" title="applied"><i class="fa fa-check" aria-hidden="true"></i>Applied</a>
                    </div>
                </div>
            </article>

            <article class="advance-search-job">
                <div class="row no-mrg">
                    <div class="col-md-6 col-sm-6">
                        <a href="new-job-detail.html" title="job Detail">
                            <div class="advance-search-img-box">
                                <img src="assets/img/com-3.jpg" class="img-responsive" alt="">
                            </div>
                        </a>
                        <div class="advance-search-caption">
                            <a href="new-job-detail.html" title="Job Dtail"><h4>PHP Developer</h4></a>
                            <span>Honda Ltd</span>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-4">
                        <div class="advance-search-job-locat">
                            <p><i class="fa fa-map-marker"></i>QBL Park, C40</p>
                        </div>
                    </div>
                    <div class="col-md-2 col-sm-2">
                        <a href="javascript:void(0)" data-toggle="modal" data-target="#apply-job" class="btn advance-search" title="apply">Apply</a>
                    </div>
                </div>
            </article>

            <article class="advance-search-job">
                <div class="row no-mrg">
                    <div class="col-md-6 col-sm-6">
                        <a href="new-job-detail.html" title="job Detail">
                            <div class="advance-search-img-box">
                                <img src="assets/img/com-1.jpg" class="img-responsive" alt="">
                            </div>
                        </a>
                        <div class="advance-search-caption">
                            <a href="new-job-detail.html" title="Job Dtail"><h4>Web Designer</h4></a>
                            <span>Autodesk Ltd</span>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-4">
                        <div class="advance-search-job-locat">
                            <p><i class="fa fa-map-marker"></i>QBL Park, C40</p>
                        </div>
                    </div>
                    <div class="col-md-2 col-sm-2">
                        <a href="#" class="btn applied advance-search" title="applied"><i class="fa fa-check" aria-hidden="true"></i>Applied</a>
                    </div>
                </div>
            </article>

        </div>



    </div>
    </section>
    <div class="clearfix"></div>







    <!-- pricing Section Start -->
    <section class="pricing">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="main-heading">
                        <p>JOB</p>
                        <h2>Job <span>search</span></h2>
                    </div>
                </div>
            </div>
            <!--/row-->

            <div class="row">

                <!-- Single Freelancer Style 2 -->
                <div class="col-md-4 col-sm-6">
                    <div class="freelance-container style-2">
                        <div class="freelance-box">
                            <span class="freelance-status">Available</span>
                            <h4 class="flc-rate">$17/hr</h4>
                            <div class="freelance-inner-box">
                                <div class="freelance-box-thumb">
                                    <img src="assets/img/can-5.jpg" class="img-responsive img-circle" alt="" />
                                </div>
                                <div class="freelance-box-detail">
                                    <h4>Agustin L. Smith</h4>
                                    <span class="location">Australia</span>
                                </div>
                                <div class="rattings">
                                    <i class="fa fa-star fill"></i>
                                    <i class="fa fa-star fill"></i>
                                    <i class="fa fa-star fill"></i>
                                    <i class="fa fa-star-half fill"></i>
                                    <i class="fa fa-star"></i>
                                </div>
                            </div>
                            <div class="freelance-box-extra">
                                <p>At vero eos et accusamus et iusto odio dignissimos ducimus qui.</p>
                                <ul>
                                    <li>Php</li>
                                    <li>Android</li>
                                    <li>Html</li>
                                    <li class="more-skill bg-primary">+3</li>
                                </ul>
                            </div>
                            <a href="freelancer-detail.html" class="btn btn-freelance-two bg-default">View Detail</a>
                            <a href="freelancer-detail.html" class="btn btn-freelance-two bg-info">View Detail</a>
                        </div>
                    </div>
                </div>

                <!-- Single Freelancer Style 2 -->
                <div class="col-md-4 col-sm-6">
                    <div class="freelance-container style-2">
                        <div class="freelance-box">
                            <span class="freelance-status bg-warning">At Work</span>
                            <h4 class="flc-rate">$22/hr</h4>
                            <div class="freelance-inner-box">
                                <div class="freelance-box-thumb">
                                    <img src="assets/img/can-5.jpg" class="img-responsive img-circle" alt="" />
                                </div>
                                <div class="freelance-box-detail">
                                    <h4>Delores R. Williams</h4>
                                    <span class="location">United States</span>
                                </div>
                                <div class="rattings">
                                    <i class="fa fa-star fill"></i>
                                    <i class="fa fa-star fill"></i>
                                    <i class="fa fa-star fill"></i>
                                    <i class="fa fa-star-half fill"></i>
                                    <i class="fa fa-star"></i>
                                </div>
                            </div>
                            <div class="freelance-box-extra">
                                <p>At vero eos et accusamus et iusto odio dignissimos ducimus qui.</p>
                                <ul>
                                    <li>Php</li>
                                    <li>Android</li>
                                    <li>Html</li>
                                    <li class="more-skill bg-primary">+3</li>
                                </ul>
                            </div>
                            <a href="freelancer-detail.html" class="btn btn-freelance-two bg-default">View Detail</a>
                            <a href="freelancer-detail.html" class="btn btn-freelance-two bg-info">View Detail</a>
                        </div>
                    </div>
                </div>

                <!-- Single Freelancer Style 2 -->
                <div class="col-md-4 col-sm-6">
                    <div class="freelance-container style-2">
                        <div class="freelance-box">
                            <span class="freelance-status">Available</span>
                            <h4 class="flc-rate">$19/hr</h4>
                            <div class="freelance-inner-box">
                                <div class="freelance-box-thumb">
                                    <img src="assets/img/can-5.jpg" class="img-responsive img-circle" alt="" />
                                </div>
                                <div class="freelance-box-detail">
                                    <h4>Daniel Disroyer</h4>
                                    <span class="location">Bangladesh</span>
                                </div>
                                <div class="rattings">
                                    <i class="fa fa-star fill"></i>
                                    <i class="fa fa-star fill"></i>
                                    <i class="fa fa-star fill"></i>
                                    <i class="fa fa-star-half fill"></i>
                                    <i class="fa fa-star"></i>
                                </div>
                            </div>
                            <div class="freelance-box-extra">
                                <p>At vero eos et accusamus et iusto odio dignissimos ducimus qui.</p>
                                <ul>
                                    <li>Php</li>
                                    <li>Android</li>
                                    <li>Html</li>
                                    <li class="more-skill bg-primary">+3</li>
                                </ul>
                            </div>
                            <a href="freelancer-detail.html" class="btn btn-freelance-two bg-default">View Detail</a>
                            <a href="freelancer-detail.html" class="btn btn-freelance-two bg-info">View Detail</a>
                        </div>
                    </div>
                </div>

            </div>

            <!-- Single Freelancer -->
            <div class="row">
                <div class="col-md-12 col-sm-12">
                    <div class="text-center">
                        <a href="freelancers-2.html" class="btn btn-primary">Load More</a>
                    </div>
                </div>
            </div>

        </div>
    </section>
    <!-- End Pricing Section -->


























    <!-- Statical-->

    <!-- video section Start -->
    <section class="video-sec dark" id="video" style="background-image:url(http://via.placeholder.com/1920x850);">
        <div class="container">
            <div class="row">
                <div class="main-heading">
                    <p>Best For Your Projects</p>
                    <h2>Watch Our <span>video</span></h2>
                </div>
            </div>
            <!--/row-->
            <div class="video-part">
                <a href="#" data-toggle="modal" data-target="#my-video" class="video-btn"><i class="fa fa-play"></i></a>
            </div>
        </div>
    </section>
    <div class="clearfix"></div>
    <!-- video section Start -->

    <!-- ====================== How It Work ================= -->
    <section class="how-it-works">
        <div class="container">

            <div class="row" data-aos="fade-up">
                <div class="col-md-12">
                    <div class="main-heading">
                        <p>Working Process</p>
                        <h2>How It <span>Works</span></h2>
                    </div>
                </div>
            </div>

            <div class="row">

                <div class="col-md-4 col-sm-4">
                    <div class="working-process">
								<span class="process-img">
									<img src="assets/img/step-1.png" class="img-responsive" alt="" />
									<span class="process-num">01</span>
								</span>
                        <h4>Create An Account</h4>
                        <p>Post a job to tell us about your project. We'll quickly match you with the right freelancers find place best.</p>
                    </div>
                </div>

                <div class="col-md-4 col-sm-4">
                    <div class="working-process">
								<span class="process-img">
									<img src="assets/img/step-2.png" class="img-responsive" alt="" />
									<span class="process-num">02</span>
								</span>
                        <h4>Search Jobs</h4>
                        <p>Post a job to tell us about your project. We'll quickly match you with the right freelancers find place best.</p>
                    </div>
                </div>

                <div class="col-md-4 col-sm-4">
                    <div class="working-process">
								<span class="process-img">
									<img src="assets/img/step-3.png" class="img-responsive" alt="" />
									<span class="process-num">03</span>
								</span>
                        <h4>Save & Apply</h4>
                        <p>Post a job to tell us about your project. We'll quickly match you with the right freelancers find place best.</p>
                    </div>
                </div>

            </div>

        </div>
    </section>
    <div class="clearfix"></div>

    <!-- testimonial section Start -->
    <section class="testimonial" id="testimonial">
        <div class="container">
            <div class="row">
                <div class="main-heading">
                    <p>What Say Our Client</p>
                    <h2>Our Success <span>Stories</span></h2>
                </div>
            </div>
            <!--/row-->
            <div class="row">
                <div id="client-testimonial-slider" class="owl-carousel">
                    <div class="client-testimonial">
                        <div class="pic">
                            <img src="http://via.placeholder.com/150x150" alt="">
                        </div>
                        <p class="client-description">
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor et dolore magna aliqua.
                        </p>
                        <h3 class="client-testimonial-title">Lacky Mole</h3>
                        <ul class="client-testimonial-rating">
                            <li class="fa fa-star-o"></li>
                            <li class="fa fa-star-o"></li>
                            <li class="fa fa-star"></li>
                        </ul>
                    </div>

                    <div class="client-testimonial">
                        <div class="pic">
                            <img src="http://via.placeholder.com/150x150" alt="">
                        </div>
                        <p class="client-description">
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor et dolore magna aliqua.
                        </p>
                        <h3 class="client-testimonial-title">Karan Wessi</h3>
                        <ul class="client-testimonial-rating">
                            <li class="fa fa-star-o"></li>
                            <li class="fa fa-star"></li>
                            <li class="fa fa-star"></li>
                        </ul>
                    </div>

                    <div class="client-testimonial">
                        <div class="pic">
                            <img src="http://via.placeholder.com/150x150" alt="">
                        </div>
                        <p class="client-description">
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor et dolore magna aliqua.
                        </p>
                        <h3 class="client-testimonial-title">Roul Pinchai</h3>
                        <ul class="client-testimonial-rating">
                            <li class="fa fa-star-o"></li>
                            <li class="fa fa-star-o"></li>
                            <li class="fa fa-star"></li>
                        </ul>
                    </div>

                    <div class="client-testimonial">
                        <div class="pic">
                            <img src="http://via.placeholder.com/150x150" alt="">
                        </div>
                        <p class="client-description">
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor et dolore magna aliqua.
                        </p>
                        <h3 class="client-testimonial-title">Adam Jinna</h3>
                        <ul class="client-testimonial-rating">
                            <li class="fa fa-star-o"></li>
                            <li class="fa fa-star-o"></li>
                            <li class="fa fa-star"></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- testimonial section End -->




@endsection
