@extends('layouts.app')

@section('content')

    <section class="inner-header-title" style="background-image:url(http://via.placeholder.com/1920x850);">
        <div class="container">
            <h1>Feedback</h1>
        </div>
    </section>


    <div class="clearfix"></div>


    <section class="pricing">
        <div class="container">

            <!--/row-->


            <div class="row">



    <div class="row no-mrg">
        <div class="comments-form">

            <form>
                <div class="col-md-6 col-sm-6">
                    <input type="text" class="form-control" placeholder="Your Name">
                </div>
                <div class="col-md-6 col-sm-6">
                    <input type="email" class="form-control" placeholder="Your Email">
                </div>
                <div class="col-md-6 col-sm-6">
                    <input type="text" class="form-control" placeholder="Your Mobile">
                </div>
                <div class="col-md-6 col-sm-6">
                    <input type="text" class="form-control" placeholder="Subject">
                </div>
                <div class="col-md-12 col-sm-12">
                    <textarea class="form-control" placeholder="Comment"></textarea>
                </div>
                <button class="thm-btn btn-comment" type="submit">Submit</button>
            </form>
        </div>
    </div>

            </div>
        </div>
        </div>


        </section>

@endsection