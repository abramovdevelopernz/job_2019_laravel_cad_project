@extends('layouts.app')

@section('content')

    <section class="inner-header-title" style="background-image:url(http://via.placeholder.com/1920x850);">
        <div class="container">
            <h1>Sign up</h1>
        </div>
    </section>


    <div class="clearfix"></div>


    <section class="pricing">
        <div class="container">

            <!--/row-->


            <div class="row">



                <div class="col-md-12 col-sm-12">
                    <div class="sidebar-wrapper">

                        <div class="sidebar-box-header bb-1">
                            <h4>Create An Account</h4>
                        </div>

                        <form class="billing-form">
                            <div class="row">
                                <div class="col-xs-12">
                                    <label>Full Name</label>
                                    <input type="text" class="form-control">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-6">
                                    <label>User Name</label>
                                    <input type="text" class="form-control">
                                </div>
                                <div class="col-xs-6">
                                    <label>Email</label>
                                    <input type="email" class="form-control">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-6">
                                    <label>Password</label>
                                    <input type="password" class="form-control">
                                </div>
                                <div class="col-xs-6">
                                    <label>Confirm Password</label>
                                    <input type="password" class="form-control">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12">
										<span class="custom-checkbox">
											<input type="checkbox" id="1">
											<label for="1"></label>
										</span> I give my consent to Job Stock to collect my data with GDPR regulation.
                                </div>
                            </div>
                            <div class="row mrg-top-30">
                                <div class="col-md-12 text-center">
                                    <a href="#" class="btn btn-success">SignUp</a>
                                    <a href="" class="btn btn-default">Cancel</a>
                                </div>
                            </div>
                        </form>

                    </div>
                </div>

            </div>
        </div>
        </div>


    </section>

@endsection