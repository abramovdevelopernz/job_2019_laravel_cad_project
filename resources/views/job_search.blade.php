@extends('layouts.app')

@section('content')

    <!-- Main Banner Section Start -->
    <div class="banner" style="background-image:url(http://via.placeholder.com/1920x850);">
        <div class="container">
            <div class="banner-caption">
                <div class="col-md-12 col-sm-12 banner-text">
                    <h1>Job search</h1>
                    <form class="form-horizontal">
                        <div class="col-md-10 no-padd">
                            <div class="input-group">
                                <input type="text" class="form-control right-bor" id="joblist" placeholder="Position">
                            </div>
                        </div>




                        <div class="col-md-2 no-padd">
                            <div class="input-group">
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <div class="company-brand">
            <div class="container">
                <div id="company-brands" class="owl-carousel">
                    <div class="brand-img">
                        <img src="assets/img/microsoft-home.png" class="img-responsive" alt="" />
                    </div>
                    <div class="brand-img">
                        <img src="assets/img/img-home.png" class="img-responsive" alt="" />
                    </div>
                    <div class="brand-img">
                        <img src="assets/img/mothercare-home.png" class="img-responsive" alt="" />
                    </div>
                    <div class="brand-img">
                        <img src="assets/img/paypal-home.png" class="img-responsive" alt="" />
                    </div>
                    <div class="brand-img">
                        <img src="assets/img/serv-home.png" class="img-responsive" alt="" />
                    </div>
                    <div class="brand-img">
                        <img src="assets/img/xerox-home.png" class="img-responsive" alt="" />
                    </div>
                    <div class="brand-img">
                        <img src="assets/img/yahoo-home.png" class="img-responsive" alt="" />
                    </div>
                    <div class="brand-img">
                        <img src="assets/img/mothercare-home.png" class="img-responsive" alt="" />
                    </div>
                </div>
            </div>
        </div>

    </div>
    <div class="clearfix"></div>
    <!-- Main Banner Section End -->



    <section class="pricing">
        <div class="container">

            <div class="row">

            <!-- Single Freelancer Style 2 -->
            <div class="col-md-4 col-sm-6">
                <div class="freelance-container style-2">
                    <div class="freelance-box">
                        <span class="freelance-status">Available</span>
                        <h4 class="flc-rate">$17/hr</h4>
                        <div class="freelance-inner-box">
                            <div class="freelance-box-thumb">
                                <img src="assets/img/can-5.jpg" class="img-responsive img-circle" alt="" />
                            </div>
                            <div class="freelance-box-detail">
                                <h4>Agustin L. Smith</h4>
                                <span class="location">Australia</span>
                            </div>
                            <div class="rattings">
                                <i class="fa fa-star fill"></i>
                                <i class="fa fa-star fill"></i>
                                <i class="fa fa-star fill"></i>
                                <i class="fa fa-star-half fill"></i>
                                <i class="fa fa-star"></i>
                            </div>
                        </div>
                        <div class="freelance-box-extra">
                            <p>At vero eos et accusamus et iusto odio dignissimos ducimus qui.</p>
                            <ul>
                                <li>Php</li>
                                <li>Android</li>
                                <li>Html</li>
                                <li class="more-skill bg-primary">+3</li>
                            </ul>
                        </div>
                        <a href="freelancer-detail.html" class="btn btn-freelance-two bg-default">View Detail</a>
                        <a href="freelancer-detail.html" class="btn btn-freelance-two bg-info">View Detail</a>
                    </div>
                </div>
            </div>

            <!-- Single Freelancer Style 2 -->
            <div class="col-md-4 col-sm-6">
                <div class="freelance-container style-2">
                    <div class="freelance-box">
                        <span class="freelance-status bg-warning">At Work</span>
                        <h4 class="flc-rate">$22/hr</h4>
                        <div class="freelance-inner-box">
                            <div class="freelance-box-thumb">
                                <img src="assets/img/can-5.jpg" class="img-responsive img-circle" alt="" />
                            </div>
                            <div class="freelance-box-detail">
                                <h4>Delores R. Williams</h4>
                                <span class="location">United States</span>
                            </div>
                            <div class="rattings">
                                <i class="fa fa-star fill"></i>
                                <i class="fa fa-star fill"></i>
                                <i class="fa fa-star fill"></i>
                                <i class="fa fa-star-half fill"></i>
                                <i class="fa fa-star"></i>
                            </div>
                        </div>
                        <div class="freelance-box-extra">
                            <p>At vero eos et accusamus et iusto odio dignissimos ducimus qui.</p>
                            <ul>
                                <li>Php</li>
                                <li>Android</li>
                                <li>Html</li>
                                <li class="more-skill bg-primary">+3</li>
                            </ul>
                        </div>
                        <a href="freelancer-detail.html" class="btn btn-freelance-two bg-default">View Detail</a>
                        <a href="freelancer-detail.html" class="btn btn-freelance-two bg-info">View Detail</a>
                    </div>
                </div>
            </div>

            <!-- Single Freelancer Style 2 -->
            <div class="col-md-4 col-sm-6">
                <div class="freelance-container style-2">
                    <div class="freelance-box">
                        <span class="freelance-status">Available</span>
                        <h4 class="flc-rate">$19/hr</h4>
                        <div class="freelance-inner-box">
                            <div class="freelance-box-thumb">
                                <img src="assets/img/can-5.jpg" class="img-responsive img-circle" alt="" />
                            </div>
                            <div class="freelance-box-detail">
                                <h4>Daniel Disroyer</h4>
                                <span class="location">Bangladesh</span>
                            </div>
                            <div class="rattings">
                                <i class="fa fa-star fill"></i>
                                <i class="fa fa-star fill"></i>
                                <i class="fa fa-star fill"></i>
                                <i class="fa fa-star-half fill"></i>
                                <i class="fa fa-star"></i>
                            </div>
                        </div>
                        <div class="freelance-box-extra">
                            <p>At vero eos et accusamus et iusto odio dignissimos ducimus qui.</p>
                            <ul>
                                <li>Php</li>
                                <li>Android</li>
                                <li>Html</li>
                                <li class="more-skill bg-primary">+3</li>
                            </ul>
                        </div>
                        <a href="freelancer-detail.html" class="btn btn-freelance-two bg-default">View Detail</a>
                        <a href="freelancer-detail.html" class="btn btn-freelance-two bg-info">View Detail</a>
                    </div>
                </div>
            </div>

        </div>






            <div class="row">
                <ul class="pagination">
                    <li><a href="#">«</a></li>
                    <li class="active"><a href="#">1</a></li>
                    <li><a href="#">2</a></li>
                    <li><a href="#">3</a></li>
                    <li><a href="#">4</a></li>
                    <li><a href="#"><i class="fa fa-ellipsis-h"></i></a></li>
                    <li><a href="#">»</a></li>
                </ul>
            </div>







        </div>
        </div>

    </section>


@endsection