@extends('layouts.app')

@section('content')



    <!-- Main Banner Section Start -->
    <div class="banner" style="background-image:url(http://via.placeholder.com/1920x850);">
        <div class="container">
            <div class="banner-caption">
                <div class="col-md-12 col-sm-12 banner-text">
                    <h1>Job wanted</h1>
                    <form class="form-horizontal">
                        <div class="col-md-10 no-padd">
                            <div class="input-group">
                                <input type="text" class="form-control right-bor" id="joblist" placeholder="Title">
                            </div>
                        </div>




                        <div class="col-md-2 no-padd">
                            <div class="input-group">
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <div class="company-brand">
            <div class="container">
                <div id="company-brands" class="owl-carousel">
                    <div class="brand-img">
                        <img src="assets/img/microsoft-home.png" class="img-responsive" alt="" />
                    </div>
                    <div class="brand-img">
                        <img src="assets/img/img-home.png" class="img-responsive" alt="" />
                    </div>
                    <div class="brand-img">
                        <img src="assets/img/mothercare-home.png" class="img-responsive" alt="" />
                    </div>
                    <div class="brand-img">
                        <img src="assets/img/paypal-home.png" class="img-responsive" alt="" />
                    </div>
                    <div class="brand-img">
                        <img src="assets/img/serv-home.png" class="img-responsive" alt="" />
                    </div>
                    <div class="brand-img">
                        <img src="assets/img/xerox-home.png" class="img-responsive" alt="" />
                    </div>
                    <div class="brand-img">
                        <img src="assets/img/yahoo-home.png" class="img-responsive" alt="" />
                    </div>
                    <div class="brand-img">
                        <img src="assets/img/mothercare-home.png" class="img-responsive" alt="" />
                    </div>
                </div>
            </div>
        </div>

    </div>
    <div class="clearfix"></div>
    <!-- Main Banner Section End -->




    <section class="pricing">
        <div class="container">

            <!--/row-->


            <div class="row">



    <div class="card-body">

    <article class="advance-search-job">
        <div class="row no-mrg">
            <div class="col-md-6 col-sm-6">
                <a href="new-job-detail.html" title="job Detail">
                    <div class="advance-search-img-box">
                        <img src="assets/img/com-2.jpg" class="img-responsive" alt="">
                    </div>
                </a>
                <div class="advance-search-caption">
                    <a href="new-job-detail.html" title="Job Dtail"><h4>Product Designer</h4></a>
                    <span>Google Ltd</span>
                </div>
            </div>
            <div class="col-md-4 col-sm-4">
                <div class="advance-search-job-locat">
                    <p><i class="fa fa-map-marker"></i>QBL Park, C40</p>
                </div>
            </div>
            <div class="col-md-2 col-sm-2">
                <a href="javascript:void(0)" data-toggle="modal" data-target="#apply-job" class="btn advance-search" title="apply">Apply</a>
            </div>
        </div>
        <span class="tg-themetag tg-featuretag">Premium</span>
    </article>

    <article class="advance-search-job">
        <div class="row no-mrg">
            <div class="col-md-6 col-sm-6">
                <a href="new-job-detail.html" title="job Detail">
                    <div class="advance-search-img-box">
                        <img src="assets/img/com-6.jpg" class="img-responsive" alt="">
                    </div>
                </a>
                <div class="advance-search-caption">
                    <a href="new-job-detail.html" title="Job Dtail"><h4>Project Manager</h4></a>
                    <span>Vertue Ltd</span>
                </div>
            </div>
            <div class="col-md-4 col-sm-4">
                <div class="advance-search-job-locat">
                    <p><i class="fa fa-map-marker"></i>QBL Park, C40</p>
                </div>
            </div>
            <div class="col-md-2 col-sm-2">
                <a href="javascript:void(0)" data-toggle="modal" data-target="#apply-job" class="btn advance-search" title="apply">Apply</a>
            </div>
        </div>
    </article>

    <article class="advance-search-job">
        <div class="row no-mrg">
            <div class="col-md-6 col-sm-6">
                <a href="new-job-detail.html" title="job Detail">
                    <div class="advance-search-img-box">
                        <img src="assets/img/com-7.jpg" class="img-responsive" alt="">
                    </div>
                </a>
                <div class="advance-search-caption">
                    <a href="new-job-detail.html" title="Job Dtail"><h4>Database Designer</h4></a>
                    <span>Twitter Ltd</span>
                </div>
            </div>
            <div class="col-md-4 col-sm-4">
                <div class="advance-search-job-locat">
                    <p><i class="fa fa-map-marker"></i>QBL Park, C40</p>
                </div>
            </div>
            <div class="col-md-2 col-sm-2">
                <a href="#" class="btn applied advance-search" title="applied"><i class="fa fa-check" aria-hidden="true"></i>Applied</a>
            </div>
        </div>
        <span class="tg-themetag tg-featuretag">Premium</span>
    </article>

    <article class="advance-search-job">
        <div class="row no-mrg">
            <div class="col-md-6 col-sm-6">
                <a href="new-job-detail.html" title="job Detail">
                    <div class="advance-search-img-box">
                        <img src="assets/img/com-2.jpg" class="img-responsive" alt="">
                    </div>
                </a>
                <div class="advance-search-caption">
                    <a href="new-job-detail.html" title="Job Dtail"><h4>Product Designer</h4></a>
                    <span>Google Ltd</span>
                </div>
            </div>
            <div class="col-md-4 col-sm-4">
                <div class="advance-search-job-locat">
                    <p><i class="fa fa-map-marker"></i>QBL Park, C40</p>
                </div>
            </div>
            <div class="col-md-2 col-sm-2">
                <a href="javascript:void(0)" data-toggle="modal" data-target="#apply-job" class="btn advance-search" title="apply">Apply</a>
            </div>
        </div>
    </article>

    <article class="advance-search-job">
        <div class="row no-mrg">
            <div class="col-md-6 col-sm-6">
                <a href="new-job-detail.html" title="job Detail">
                    <div class="advance-search-img-box">
                        <img src="assets/img/com-5.jpg" class="img-responsive" alt="">
                    </div>
                </a>
                <div class="advance-search-caption">
                    <a href="new-job-detail.html" title="Job Dtail"><h4>Human Resource</h4></a>
                    <span>Skype Ltd</span>
                </div>
            </div>
            <div class="col-md-4 col-sm-4">
                <div class="advance-search-job-locat">
                    <p><i class="fa fa-map-marker"></i>QBL Park, C40</p>
                </div>
            </div>
            <div class="col-md-2 col-sm-2">
                <a href="#" class="btn applied advance-search" title="applied"><i class="fa fa-check" aria-hidden="true"></i>Applied</a>
            </div>
        </div>
    </article>

    <article class="advance-search-job">
        <div class="row no-mrg">
            <div class="col-md-6 col-sm-6">
                <a href="new-job-detail.html" title="job Detail">
                    <div class="advance-search-img-box">
                        <img src="assets/img/com-6.jpg" class="img-responsive" alt="">
                    </div>
                </a>
                <div class="advance-search-caption">
                    <a href="new-job-detail.html" title="Job Dtail"><h4>Project Manager</h4></a>
                    <span>Vertue Ltd</span>
                </div>
            </div>
            <div class="col-md-4 col-sm-4">
                <div class="advance-search-job-locat">
                    <p><i class="fa fa-map-marker"></i>QBL Park, C40</p>
                </div>
            </div>
            <div class="col-md-2 col-sm-2">
                <a href="javascript:void(0)" data-toggle="modal" data-target="#apply-job" class="btn advance-search" title="applied">Apply</a>
            </div>
        </div>
        <span class="tg-themetag tg-featuretag">Premium</span>
    </article>

    <article class="advance-search-job">
        <div class="row no-mrg">
            <div class="col-md-6 col-sm-6">
                <a href="new-job-detail.html" title="job Detail">
                    <div class="advance-search-img-box">
                        <img src="assets/img/com-7.jpg" class="img-responsive" alt="">
                    </div>
                </a>
                <div class="advance-search-caption">
                    <a href="new-job-detail.html" title="Job Dtail"><h4>CEO &amp; Manager</h4></a>
                    <span>Twitter</span>
                </div>
            </div>
            <div class="col-md-4 col-sm-4">
                <div class="advance-search-job-locat">
                    <p><i class="fa fa-map-marker"></i>QBL Park, C40</p>
                </div>
            </div>
            <div class="col-md-2 col-sm-2">
                <a href="javascript:void(0)" data-toggle="modal" data-target="#apply-job" class="btn advance-search" title="apply">Apply</a>
            </div>
        </div>
    </article>

    <article class="advance-search-job">
        <div class="row no-mrg">
            <div class="col-md-6 col-sm-6">
                <a href="new-job-detail.html" title="job Detail">
                    <div class="advance-search-img-box">
                        <img src="assets/img/com-4.jpg" class="img-responsive" alt="">
                    </div>
                </a>
                <div class="advance-search-caption">
                    <a href="new-job-detail.html" title="Job Dtail"><h4>Product Designer</h4></a>
                    <span>Microsoft Ltd</span>
                </div>
            </div>
            <div class="col-md-4 col-sm-4">
                <div class="advance-search-job-locat">
                    <p><i class="fa fa-map-marker"></i>QBL Park, C40</p>
                </div>
            </div>
            <div class="col-md-2 col-sm-2">
                <a href="#" class="btn applied advance-search" title="applied"><i class="fa fa-check" aria-hidden="true"></i>Applied</a>
            </div>
        </div>
    </article>

    <article class="advance-search-job">
        <div class="row no-mrg">
            <div class="col-md-6 col-sm-6">
                <a href="new-job-detail.html" title="job Detail">
                    <div class="advance-search-img-box">
                        <img src="assets/img/com-3.jpg" class="img-responsive" alt="">
                    </div>
                </a>
                <div class="advance-search-caption">
                    <a href="new-job-detail.html" title="Job Dtail"><h4>PHP Developer</h4></a>
                    <span>Honda Ltd</span>
                </div>
            </div>
            <div class="col-md-4 col-sm-4">
                <div class="advance-search-job-locat">
                    <p><i class="fa fa-map-marker"></i>QBL Park, C40</p>
                </div>
            </div>
            <div class="col-md-2 col-sm-2">
                <a href="javascript:void(0)" data-toggle="modal" data-target="#apply-job" class="btn advance-search" title="apply">Apply</a>
            </div>
        </div>
    </article>

    <article class="advance-search-job">
        <div class="row no-mrg">
            <div class="col-md-6 col-sm-6">
                <a href="new-job-detail.html" title="job Detail">
                    <div class="advance-search-img-box">
                        <img src="assets/img/com-1.jpg" class="img-responsive" alt="">
                    </div>
                </a>
                <div class="advance-search-caption">
                    <a href="new-job-detail.html" title="Job Dtail"><h4>Web Designer</h4></a>
                    <span>Autodesk Ltd</span>
                </div>
            </div>
            <div class="col-md-4 col-sm-4">
                <div class="advance-search-job-locat">
                    <p><i class="fa fa-map-marker"></i>QBL Park, C40</p>
                </div>
            </div>
            <div class="col-md-2 col-sm-2">
                <a href="#" class="btn applied advance-search" title="applied"><i class="fa fa-check" aria-hidden="true"></i>Applied</a>
            </div>
        </div>
    </article>

    </div>





            </div>


            <div class="row">
                <ul class="pagination">
                    <li><a href="#">«</a></li>
                    <li class="active"><a href="#">1</a></li>
                    <li><a href="#">2</a></li>
                    <li><a href="#">3</a></li>
                    <li><a href="#">4</a></li>
                    <li><a href="#"><i class="fa fa-ellipsis-h"></i></a></li>
                    <li><a href="#">»</a></li>
                </ul>
            </div>





        </div>
        </div>

        </section>


@endsection