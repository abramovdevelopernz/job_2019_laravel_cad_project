@extends('layouts.app')

@section('content')

    <!-- Main Banner Section Start -->
    <div class="banner" style="background-image:url(http://via.placeholder.com/1920x850);">
        <div class="container">
            <div class="banner-caption">
                <div class="col-md-12 col-sm-12 banner-text">
                    <h1>Directory</h1>
                    <form class="form-horizontal">
                        <div class="col-md-7 no-padd">
                            <div class="input-group">
                                <input type="text" class="form-control right-bor" id="joblist" placeholder="Company name">
                            </div>
                        </div>


                        {{--

                        <div class="col-md-3 no-padd">
                            <div class="input-group">
                                <input type="text" class="form-control right-bor" id="location" placeholder="Search By Location..">
                            </div>
                        </div>
--}}
                        <div class="col-md-3 no-padd">
                            <div class="input-group">
                                <select id="choose-city" class="form-control">
                                    <option>Choose Region</option>
                                    <option>Christchurch</option>
                                    <option>Wellington</option>
                                    <option>Auckland</option>
                                </select>
                            </div>
                        </div>

                        <div class="col-md-2 no-padd">
                            <div class="input-group">
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <div class="company-brand">
            <div class="container">
                <div id="company-brands" class="owl-carousel">
                    <div class="brand-img">
                        <img src="assets/img/microsoft-home.png" class="img-responsive" alt="" />
                    </div>
                    <div class="brand-img">
                        <img src="assets/img/img-home.png" class="img-responsive" alt="" />
                    </div>
                    <div class="brand-img">
                        <img src="assets/img/mothercare-home.png" class="img-responsive" alt="" />
                    </div>
                    <div class="brand-img">
                        <img src="assets/img/paypal-home.png" class="img-responsive" alt="" />
                    </div>
                    <div class="brand-img">
                        <img src="assets/img/serv-home.png" class="img-responsive" alt="" />
                    </div>
                    <div class="brand-img">
                        <img src="assets/img/xerox-home.png" class="img-responsive" alt="" />
                    </div>
                    <div class="brand-img">
                        <img src="assets/img/yahoo-home.png" class="img-responsive" alt="" />
                    </div>
                    <div class="brand-img">
                        <img src="assets/img/mothercare-home.png" class="img-responsive" alt="" />
                    </div>
                </div>
            </div>
        </div>

    </div>
    <div class="clearfix"></div>
    <!-- Main Banner Section End -->


    <section class="pricing">
        <div class="container">
            <div class="row">

                <div id="directory"></div>

            </div>

            <div class="row">
                <ul class="pagination">
                    <li><a href="#">«</a></li>
                    <li class="active"><a href="#">1</a></li>
                    <li><a href="#">2</a></li>
                    <li><a href="#">3</a></li>
                    <li><a href="#">4</a></li>
                    <li><a href="#"><i class="fa fa-ellipsis-h"></i></a></li>
                    <li><a href="#">»</a></li>
                </ul>
            </div>

        </div>
    </section>



@endsection