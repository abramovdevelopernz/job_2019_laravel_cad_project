<!doctype html>
    <html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <title>{{ config('app.name', 'Laravel') }}</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"  >
        <meta name="csrf-token" content="{{ csrf_token() }}">
        @include('includes.styles')
    </head>

    <body>
    <div class="Loader"></div>
    <div class="wrapper">

    @include('includes.header')
    @include('includes.messages')
    @yield('content')
    @include('includes.footer')
    @include('includes.javascript')

    </div>
</body>
</html>