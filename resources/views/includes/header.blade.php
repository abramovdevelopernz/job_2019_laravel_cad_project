<!-- Start Navigation -->
<nav class="navbar navbar-default navbar-fixed navbar-transparent white bootsnav">

    <div class="container">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-menu">
            <i class="fa fa-bars"></i>
        </button>
        <!-- Start Header Navigation -->
        <div class="navbar-header">
            <a class="navbar-brand" href="/">
                <img src="assets/img/logo.png" class="logo logo-display" alt="">
                <img src="assets/img/logo-white.png" class="logo logo-scrolled" alt="">
            </a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="navbar-menu">
            <ul class="nav navbar-nav navbar-right" data-in="fadeInDown" data-out="fadeOutUp">
                <li><a href="/signup"><i class="fa fa-pencil" aria-hidden="true"></i>SignUp</a></li>
                <li><a href="/pricing"><i class="fa fa-sign-in" aria-hidden="true"></i>Pricing</a></li>
                <li class="left-br"><a href="javascript:void(0)"  data-toggle="modal" data-target="#signup" class="signin">Sign In</a></li>
            </ul>
            <ul class="nav navbar-nav navbar-right" data-in="fadeInDown" data-out="fadeOutUp">

                <li><a href="/news">News</a></li>
                <li><a href="/directory">Directory</a></li>
                <li><a href="/tender">Tender</a></li>




                <li class="dropdown megamenu-fw"><a href="#" class="dropdown-toggle" data-toggle="dropdown">Job</a>
                    <ul class="dropdown-menu megamenu-content" role="menu">
                        <li>
                            <div class="row">
                                <div class="col-menu col-md-3">
                                    <h6 class="title">Job</h6>
                                    <div class="content">
                                        <ul class="menu-col">
                                            <li><a href="/job_wanted">Wanted</a></li>
                                            <li><a href="/job_search">Job search</a></li>
                                        </ul>
                                    </div>
                                </div><!-- end col-3 -->
                            </div><!-- end row -->
                        </li>
                    </ul>
                </li>



                <li class="dropdown megamenu-fw"><a href="#" class="dropdown-toggle" data-toggle="dropdown">Equipment</a>
                    <ul class="dropdown-menu megamenu-content" role="menu">
                        <li>
                            <div class="row">
                                <div class="col-menu col-md-3">
                                    <h6 class="title">Equipment</h6>
                                    <div class="content">
                                        <ul class="menu-col">
                                            <li><a href="/sale">Sale</a></li>
                                            <li><a href="/hire">Hire</a></li>
                                            <li><a href="/rent">Rent</a></li>
                                            <li><a href="/buy">Buy</a></li>
                                        </ul>
                                    </div>
                                </div><!-- end col-3 -->
                            </div><!-- end row -->
                        </li>
                    </ul>
                </li>

                <!--  <li><a href="/training">Training</a></li>
 -->

             </ul>
         </div><!-- /.navbar-collapse -->
    </div>
</nav>
<!-- End Navigation -->


<div class="clearfix"></div>
