@extends('layouts.app')

@section('content')



    <!-- Main Banner Section Start -->
    <div class="banner" style="background-image:url(http://via.placeholder.com/1920x850);">
        <div class="container">
            <div class="banner-caption">
                <div class="col-md-12 col-sm-12 banner-text">
                    <h1>Tender</h1>
                    <form class="form-horizontal">
                        <div class="col-md-7 no-padd">
                            <div class="input-group">
                                <input type="text" class="form-control right-bor" id="joblist" placeholder="Title">
                            </div>
                        </div>


                        {{--

                        <div class="col-md-3 no-padd">
                            <div class="input-group">
                                <input type="text" class="form-control right-bor" id="location" placeholder="Search By Location..">
                            </div>
                        </div>
--}}
                        <div class="col-md-3 no-padd">
                            <div class="input-group">
                                <select id="choose-city" class="form-control">
                                    <option>Choose Region</option>
                                    <option>Christchurch</option>
                                    <option>Wellington</option>
                                    <option>Auckland</option>
                                </select>
                            </div>
                        </div>

                        <div class="col-md-2 no-padd">
                            <div class="input-group">
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <div class="company-brand">
            <div class="container">
                <div id="company-brands" class="owl-carousel">
                    <div class="brand-img">
                        <img src="assets/img/microsoft-home.png" class="img-responsive" alt="" />
                    </div>
                    <div class="brand-img">
                        <img src="assets/img/img-home.png" class="img-responsive" alt="" />
                    </div>
                    <div class="brand-img">
                        <img src="assets/img/mothercare-home.png" class="img-responsive" alt="" />
                    </div>
                    <div class="brand-img">
                        <img src="assets/img/paypal-home.png" class="img-responsive" alt="" />
                    </div>
                    <div class="brand-img">
                        <img src="assets/img/serv-home.png" class="img-responsive" alt="" />
                    </div>
                    <div class="brand-img">
                        <img src="assets/img/xerox-home.png" class="img-responsive" alt="" />
                    </div>
                    <div class="brand-img">
                        <img src="assets/img/yahoo-home.png" class="img-responsive" alt="" />
                    </div>
                    <div class="brand-img">
                        <img src="assets/img/mothercare-home.png" class="img-responsive" alt="" />
                    </div>
                </div>
            </div>
        </div>

    </div>
    <div class="clearfix"></div>
    <!-- Main Banner Section End -->



    <section class="pricing">
        <div class="container">

            <!--/row-->


            <div class="row">


                <div class="item-click">
                    <article>
                        <div class="brows-job-list">
                            <div class="col-md-1 col-sm-2 small-padding">
                                <div class="brows-job-company-img">
                                    <a href="job-detail.html"><img src="http://via.placeholder.com/150x150" class="img-responsive" alt=""></a>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-5">
                                <div class="brows-job-position">
                                    <a href="job-apply-detail.html"><h3>Digital Marketing Manager</h3></a>
                                    <p>
                                        <span>Autodesk</span><span class="brows-job-sallery"><i class="fa fa-money"></i>$750 - 800</span>
                                        <span class="job-type cl-success bg-trans-success">Full Time</span>
                                    </p>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-3">
                                <div class="brows-job-location">
                                    <p><i class="fa fa-map-marker"></i>QBL Park, C40</p>
                                </div>
                            </div>
                            <div class="col-md-2 col-sm-2">
                                <div class="brows-job-link">
                                    <a href="job-apply-detail.html" class="btn btn-default">Apply Now</a>
                                </div>
                            </div>
                        </div>
                        <span class="tg-themetag tg-featuretag">Premium</span>
                    </article>
                </div>

                <div class="item-click">
                    <article>
                        <div class="brows-job-list">
                            <div class="col-md-1 col-sm-2 small-padding">
                                <div class="brows-job-company-img">
                                    <a href="job-detail.html"><img src="http://via.placeholder.com/150x150" class="img-responsive" alt=""></a>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-5">
                                <div class="brows-job-position">
                                    <a href="job-apply-detail.html"><h3>Compensation Analyst</h3></a>
                                    <p>
                                        <span>Google</span><span class="brows-job-sallery"><i class="fa fa-money"></i>$810 - 900</span>
                                        <span class="job-type bg-trans-warning cl-warning">Part Time</span>
                                    </p>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-3">
                                <div class="brows-job-location">
                                    <p><i class="fa fa-map-marker"></i>QBL Park, C40</p>
                                </div>
                            </div>
                            <div class="col-md-2 col-sm-2">
                                <div class="brows-job-link">
                                    <a href="job-apply-detail.html" class="btn btn-default">Apply Now</a>
                                </div>
                            </div>
                        </div>
                    </article>
                </div>

                <div class="item-click">
                    <article>
                        <div class="brows-job-list">
                            <div class="col-md-1 col-sm-2 small-padding">
                                <div class="brows-job-company-img">
                                    <a href="job-detail.html"><img src="http://via.placeholder.com/150x150" class="img-responsive" alt=""></a>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-5">
                                <div class="brows-job-position">
                                    <a href="job-apply-detail.html"><h3>Investment Banker</h3></a>
                                    <p>
                                        <span>Honda</span><span class="brows-job-sallery"><i class="fa fa-money"></i>$800 - 910</span>
                                        <span class="job-type bg-trans-primary cl-primary">Freelancer</span>
                                    </p>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-3">
                                <div class="brows-job-location">
                                    <p><i class="fa fa-map-marker"></i>QBL Park, C40</p>
                                </div>
                            </div>
                            <div class="col-md-2 col-sm-2">
                                <div class="brows-job-link">
                                    <a href="job-apply-detail.html" class="btn btn-default">Apply Now</a>
                                </div>
                            </div>
                        </div>
                        <span class="tg-themetag tg-featuretag">Premium</span>
                    </article>
                </div>

                <div class="item-click">
                    <article>
                        <div class="brows-job-list">
                            <div class="col-md-1 col-sm-2 small-padding">
                                <div class="brows-job-company-img">
                                    <a href="job-detail.html"><img src="http://via.placeholder.com/150x150" class="img-responsive" alt=""></a>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-5">
                                <div class="brows-job-position">
                                    <a href="job-apply-detail.html"><h3>Financial Analyst</h3></a>
                                    <p>
                                        <span>Microsoft</span><span class="brows-job-sallery"><i class="fa fa-money"></i>$580 - 600</span>
                                        <span class="job-type bg-trans-success cl-success">Full Time</span>
                                    </p>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-3">
                                <div class="brows-job-location">
                                    <p><i class="fa fa-map-marker"></i>QBL Park, C40</p>
                                </div>
                            </div>
                            <div class="col-md-2 col-sm-2">
                                <div class="brows-job-link">
                                    <a href="job-apply-detail.html" class="btn btn-default">Apply Now</a>
                                </div>
                            </div>
                        </div>
                    </article>
                </div>







            </div>



















            <div class="row">
                <ul class="pagination">
                    <li><a href="#">«</a></li>
                    <li class="active"><a href="#">1</a></li>
                    <li><a href="#">2</a></li>
                    <li><a href="#">3</a></li>
                    <li><a href="#">4</a></li>
                    <li><a href="#"><i class="fa fa-ellipsis-h"></i></a></li>
                    <li><a href="#">»</a></li>
                </ul>
            </div>

        </div>
    </section>

@endsection