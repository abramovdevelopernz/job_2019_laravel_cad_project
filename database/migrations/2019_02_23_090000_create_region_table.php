<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRegionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('regions', function (Blueprint $table) {
            $table->increments('id');
            $table->string('region');
            $table->timestamps();
        });


        DB::table('regions')->insert(
            array(
                'region' => 'Christchirch'
            )
        );

        DB::table('regions')->insert(
            array(
                'region' => 'Wellington'
            )
        );


        DB::table('regions')->insert(
            array(
                'region' => 'Auckland'
            )
        );


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('regions');
    }
}
