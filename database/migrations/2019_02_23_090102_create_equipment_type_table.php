<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEquipmentTypeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('equipment_types', function (Blueprint $table) {
            $table->increments('id');
            $table->string('equipment_type');
            $table->timestamps();
        });


        DB::table('equipment_types')->insert(
            array(
                'equipment_type' => 'Hire'
            )
        );

        DB::table('equipment_types')->insert(
            array(
                'equipment_type' => 'Sale'
            )
        );

        DB::table('equipment_types')->insert(
            array(
                'equipment_type' => 'Rent'
            )
        );

        DB::table('equipment_types')->insert(
            array(
                'equipment_type' => 'Buy'
            )
        );

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('equipment_types');
    }
}
