<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJobTypeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('job_types', function (Blueprint $table) {
            $table->increments('id');
            $table->string('job_type');
            $table->timestamps();
        });

        DB::table('job_types')->insert(
            array(
                'job_type' => 'Wanted'
            )
        );

        DB::table('job_types')->insert(
            array(
                'job_type' => 'Job search'
            )
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('job_types');
    }
}
