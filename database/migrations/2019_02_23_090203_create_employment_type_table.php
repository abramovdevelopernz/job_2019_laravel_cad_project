<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmploymentTypeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employment_types', function (Blueprint $table) {
            $table->increments('id');
            $table->string('employment_type');
            $table->timestamps();
        });


        DB::table('employment_types')->insert(
            array(
                'employment_type' => '-'
            )
        );

        DB::table('employment_types')->insert(
            array(
                'employment_type' => 'Part time'
            )
        );

        DB::table('employment_types')->insert(
            array(
                'employment_type' => 'Full time'
            )
        );

        DB::table('employment_types')->insert(
            array(
                'employment_type' => 'Contract'
            )
        );

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employment_types');
    }
}
