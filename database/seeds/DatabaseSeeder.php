<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {

        factory(App\User::class,100)->create();
        factory(App\Tender::class,100)->create();
        factory(App\News::class,100)->create();
        factory(App\Training::class,100)->create();
        factory(App\Job::class,100)->create();
        factory(App\Equipment::class,100)->create();

    }
}
