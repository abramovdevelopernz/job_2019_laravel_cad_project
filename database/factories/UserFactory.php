<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
https://github.com/fzaninotto/Faker
*/

$factory->define(App\User::class, function (Faker $faker) {
    return [
        'email' => $faker->unique()->safeEmail,
        'password' => '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', // secret
        'user_type_id' => mt_rand(1, 2),
        'name' => $faker->company,
        'notes' => $faker->text,
        'region_id' => mt_rand(1, 3),
        'city' => $faker->city,
        'address' => $faker->address,
        'phone' =>  $faker->phoneNumber,
         //'021'.mt_rand(00000000, 99999999),
        'website' => 'http://'.$faker->domainName,
        'email_verified_at' => now(),
        'remember_token' => str_random(10),
    ];
});

$factory->define(App\Equipment::class, function (Faker $faker) {
    return [
        'user_id' => mt_rand(1, 100),
        'equipment_type_id' => mt_rand(2, 4),
        'equipment' => str_random(10),
        'notes' => $faker->text,
        'price' => mt_rand(0, 9999),
    ];
});

$factory->define(App\Tender::class, function (Faker $faker) {
    return [
        'user_id' => mt_rand(1, 100),
        'title' => $faker->catchPhrase,
        'region_id' => mt_rand(1, 3),
        'notes' => $faker->text,
    ];
});

$factory->define(App\News::class, function (Faker $faker) {
    return [
        'user_id' => mt_rand(1, 100),
        'title' => $faker->catchPhrase,
        'text' => $faker->text,
    ];
});

$factory->define(App\Training::class, function (Faker $faker) {
    return [
        'user_id' => mt_rand(1, 100),
        'title' => $faker->catchPhrase,
        'text' => $faker->text,
    ];
});

$factory->define(App\Job::class, function (Faker $faker) {
    return [
        'user_id' => mt_rand(1, 100),
        'job_type_id' => mt_rand(1, 2),
        'notes' => $faker->text,
        'employment_type_id' => mt_rand(2, 4),
        'job_vacancy_title_specialist' => $faker->jobTitle,
    ];
});

