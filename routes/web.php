<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/directory', function () {
    return view('directory');
});


Route::get('/directory_list_load_more/', 'User_Controller@directory_list_load_more');




Route::get('/test', function () {
    return view('test');
});


//Вывод списка регионов для JSON
Route::get('/region_list/', 'Region_Controller@region_list');



//https://www.youtube.com/watch?v=koQmm5HIFgw
//http://127.0.0.1:8000/directory_pagination_test
Route::get('/directory_pagination_test', function () {
    return view('directory_pagination_test');
});
Route::get('/directory_pagination_list/', 'User_Controller@index_pagination');

//https://www.youtube.com/watch?v=koQmm5HIFgw
//http://127.0.0.1:8000/directory_pagination_test_load_more
Route::get('/directory_pagination_test_load_more', function () {
    return view('directory_pagination_test_load_more');
});
Route::get('/directory_pagination_list_load_more/', 'User_Controller@index_pagination_load_more');






Route::get('/directory_test', function () {
    return view('directory_test');
});
Route::get('/directory_list/', 'User_Controller@index');





Route::get('/', function () {
    return view('welcome');
});

Route::get('/news', function () {
    return view('news');
});








Route::get('/tender', function () {
    return view('tender');
});

Route::get('/feedback', function () {
    return view('feedback');
});

Route::get('/job_search', function () {
    return view('job_search');
});

Route::get('/job_wanted', function () {
    return view('job_wanted');
});

Route::get('/training', function () {
    return view('training');
});

Route::get('/faq', function () {
    return view('faq');
});

Route::get('/contact', function () {
    return view('contact');
});

Route::get('/pricing', function () {
    return view('pricing');
});

Route::get('/signup', function () {
    return view('signup');
});


Route::get('/employment_type/', 'Employment_type_Controller@index');

Route::get('/directory/show/{id}', 'User_Controller@show');


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
